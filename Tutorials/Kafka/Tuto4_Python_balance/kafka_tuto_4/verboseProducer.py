from kafka import KafkaProducer
from time import sleep

producer = KafkaProducer(bootstrap_servers=['localhost:9092'])
#producer = KafkaProducer(bootstrap_servers=['localhost:9092'], metadata_max_age_ms=20000)

for i in range(10000):
    message = "message number" + str(i)
    partition_id = producer.send(topic='test', value=message.encode("utf-8")).get().partition
    print("sent:" + message + " to partition " + str(partition_id))
    sleep(1)
      
