from kafka import KafkaConsumer

consumer = KafkaConsumer('test',
                         bootstrap_servers=['localhost:9092'], 
                         auto_offset_reset='earliest',
                         group_id='python-group')
#consumer = KafkaConsumer('test', bootstrap_servers=['localhost:9092'], auto_offset_reset='earliest', group_id='python-group', metadata_max_age_ms=20000)

try:
    for message in consumer:
        print("received: " + message.value.decode("utf-8") + " from partition " + str(message.partition))

except KeyboardInterrupt:
    pass

finally:
    consumer.close()
