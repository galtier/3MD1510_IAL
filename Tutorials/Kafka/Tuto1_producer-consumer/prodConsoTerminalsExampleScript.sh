#!/bin/bash

# This is an example on how to launch a terminal with multiple tabs, give each tab a title and execute a command in each tab (Zookeeper, Kafka broker, topic creation, kafka consumer, kafka producer).
# --default-working-directory
# --hold is to keep the tab open after the command is finished.
# --command

# wait 2 seconds to start the kafka broker after Zookeeper was started in the previous tab,
# wait 5 seconds to create the test topic
# wait 7 seconds to start the default console consumer and producer

# trailing /bin/bash is to provide a bash interpreter after the previous command is finished

WORKING_DIR=$(pwd)
xfce4-terminal --default-working-directory=$WORKING_DIR \
--tab --title=Zookeeper --hold --command="/bin/bash -c '$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties'" \
--tab --title=KafkaBroker --hold --command="/bin/bash -c 'sleep 2s ; $KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties ; /bin/bash'" \
--tab --title=Topic --hold --command="/bin/bash -c 'sleep 5s ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test ; /bin/bash'" \
--tab --title=Consumer --hold --command="/bin/bash -c 'sleep 7s ; $KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning --consumer-property group.id=testconsumers ; /bin/bash'" \
--tab --title=Producer --hold --command="/bin/bash -c ' sleep 7s ; $KAFKA_HOME/bin/kafka-console-producer.sh --bootstrap-server localhost:9092 --topic test ; /bin/bash'"

