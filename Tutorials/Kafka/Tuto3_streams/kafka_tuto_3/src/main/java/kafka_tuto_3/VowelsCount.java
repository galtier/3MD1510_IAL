package kafka_tuto_3;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Produced;

public class VowelsCount {
	public static void main(final String[] args) {
		new VowelsCount();
	}

	public VowelsCount() {
		Topology vowelsCountTopology = createVowelsCountTopology();
		KafkaStreams vowelsCountStream = new KafkaStreams(vowelsCountTopology, configureVowelsCount());
		vowelsCountStream.start();
	}

	private Topology createVowelsCountTopology() {
		StreamsBuilder builder = new StreamsBuilder();

		// creates a KStream from the input-topic
		KStream<String, String> source = builder.stream("input-topic");
		// materializes this KStream to the output-topic
		// source.to("output-topic");

		// creates a KStream with upper-case records
		KStream<String, String> upperCaseStream = source
				.map(new KeyValueMapper<String, String, KeyValue<String, String>>() {
					@Override
					public KeyValue<String, String> apply(String key, String value) {
						return new KeyValue<>(key, value.toUpperCase());
					}

				});
		// materializes this KStream to the output-topic
		// upperCaseStream.to("output-topic");

		// creates a KStream of characters
		KStream<String, String> charactersStream = upperCaseStream
				.flatMap(new KeyValueMapper<String, String, Iterable<KeyValue<String, String>>>() {
					@Override
					public Iterable<KeyValue<String, String>> apply(String key, String value) {
						char[] characters = value.toCharArray();
						List<KeyValue<String, String>> result = new ArrayList<>(characters.length);
						for (char character : characters) {
							result.add(new KeyValue<>(key, "" + character));
						}
						return result;
					}
				});
		// materializes this KStream to the output-topic
		// charactersStream.to("output-topic");

		// keeps only the vowels
		KStream<String, String> vowelsStream = charactersStream
				.filter(new Predicate<String, String>() {
					@Override
					public boolean test(String key, String value) {
						return "AEIOUY".contains(value);
					}
				});
		// materializes this KStream to the output-topic
		// vowelsStream.to("output-topic");

		// GROUP AND COUNT
		// First, we group records by value (which contains the letter):
		KGroupedStream<String, String> vowelsGroupedStream = vowelsStream
				.groupBy(new KeyValueMapper<String, String, String>() {
					public String apply(String key, String value) {
						return value;
					}
				});
		// Second, we feed a KTable which holds the count for each group:
		KTable<String, Long> vowelsCountTable = vowelsGroupedStream.count();
		// Each time the table gets updated, we want the updated row to be sent to a new KStream:
		KStream<String, Long> vowelsCountingStreams = vowelsCountTable.toStream();
		// The types of the records in this KStream are 
		//     String for the key (since the key is now the letter, because of our selector for the groupBy operation)
		//     and Long for the value (since the value is the number of occurrences of the letter).
		// When we want to materialize the record back to the output-topic, we need to specify the new Serde:
		vowelsCountingStreams.to("output-topic", Produced.with(Serdes.String(), Serdes.Long()));
		return builder.build();
	}

	private Properties configureVowelsCount() {
		Properties vowelsCountStreamsProperties = new Properties();
		vowelsCountStreamsProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "vowelsCount");
		vowelsCountStreamsProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		// The semantics of caching is that data is flushed to the state store and
		// forwarded to the next downstream processor node whenever the earliest of
		// commit.interval.ms or cache.max.bytes.buffering (cache pressure) hits.
		vowelsCountStreamsProperties.put(StreamsConfig.STATESTORE_CACHE_MAX_BYTES_CONFIG, 0);
		vowelsCountStreamsProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		vowelsCountStreamsProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		vowelsCountStreamsProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		return vowelsCountStreamsProperties;
	}
}