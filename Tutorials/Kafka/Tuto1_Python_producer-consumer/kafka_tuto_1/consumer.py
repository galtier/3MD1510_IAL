from kafka import KafkaConsumer

consumer = KafkaConsumer('test',
                         bootstrap_servers=['localhost:9092'], 
                         auto_offset_reset='earliest',
                         group_id='python-group')

for message in consumer:
    print("received: " + message.value.decode("utf-8"))