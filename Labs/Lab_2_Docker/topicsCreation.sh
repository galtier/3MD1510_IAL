echo "delete wikimedia topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic wikimedia
echo "create wikimedia topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 2 --config retention.ms=3600000 --topic wikimedia

echo "delete region topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic region
echo "create region topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic region
