#!/bin/bash

WORKING_DIR=$(pwd)
xfce4-terminal --default-working-directory=$WORKING_DIR \
--tab --title=T1Zookeeper --hold --command="/bin/bash -c '$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties; /bin/bash'" \
--tab --title=T2KafkaBroker0 --hold --command="/bin/bash -c 'sleep 2s ; $KAFKA_HOME/bin/kafka-server-start.sh server-0.properties ; /bin/bash'" \
--tab --title=T3KafkaBroker1 --hold --command="/bin/bash -c 'sleep 2s ; $KAFKA_HOME/bin/kafka-server-start.sh server-1.properties ; /bin/bash'" \
--tab --title=T4Topics --hold --command="/bin/bash -c 'sleep 5s ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 2 --config retention.ms=3600000 --topic wikimedia ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --config retention.ms=3600000 --topic region ;/bin/bash'" \
--tab --title=T5Proxy --hold --command="/bin/bash -c ' sleep 7s ; java -jar Proxy-jar-with-dependencies.jar localhost:9092 wikimedia ; /bin/bash'" \
--tab --title=T6Filter1 --hold --command="/bin/bash -c ' sleep 7s ; java -jar Filter-jar-with-dependencies.jar localhost:9092 wikimedia region ; /bin/bash'"  \
--tab --title=T7Filter2 --hold --command="/bin/bash -c ' sleep 7s ; java -jar Filter-jar-with-dependencies.jar localhost:9092 wikimedia region ; /bin/bash'"  \
--tab --title=T8Aggregator --hold --command="/bin/bash -c ' sleep 7s ; java -jar Aggregator-jar-with-dependencies.jar localhost:9092 region 30 ; /bin/bash'"

