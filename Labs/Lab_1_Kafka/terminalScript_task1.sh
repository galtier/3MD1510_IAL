#!/bin/bash

# wait 2 seconds to start the kafka broker after Zookeeper was started in the previous tab,
# wait 5 seconds to create the wikimedia topic,
# wait 7 seconds to start the default console consumer,
# you can use the last tab to launch the Proxy

# trailing /bin/bash is to provide a bash interpreter after the previous command is finished

WORKING_DIR=$(pwd)
xfce4-terminal --default-working-directory=$WORKING_DIR \
--tab --title=Zookeeper --hold --command="/bin/bash -c '$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties'" \
--tab --title=KafkaBroker --hold --command="/bin/bash -c 'sleep 2s ; $KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties ; /bin/bash'" \
--tab --title=Topic --hold --command="/bin/bash -c 'sleep 5s ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic wikimedia ; /bin/bash'" \
--tab --title=Consumer --hold --command="/bin/bash -c 'sleep 7s ; $KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic wikimedia --from-beginning ; /bin/bash'" \
--tab --title=Proxy --hold --working-directory=./ --command="/bin/bash -c ' sleep 7s ; /bin/bash'"

