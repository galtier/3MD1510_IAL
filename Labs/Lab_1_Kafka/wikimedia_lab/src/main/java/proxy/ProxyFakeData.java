package proxy;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 * For debugging purpose, 
 * posts a fixed set of 20 wikipedia changes to the wikimedia topic, 1 per second
 */

public class ProxyFakeData {
	public static void main(String[] args) {
		new ProxyFakeData();
	}

	ProxyFakeData() {
		KafkaProducer<Void, String> kafkaProducer;
		kafkaProducer = new KafkaProducer<Void, String>(configureKafkaProducer());

		String[] messages = new String[5];
		messages[0] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";
		messages[1] = "{\"meta\":{\"domain\":\"fr.wikipedia.org\"}}";
		messages[2] = "{\"meta\":{\"domain\":\"it.wikipedia.org\"}}";
		messages[3] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";
		messages[4] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";
		messages[5] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";
		messages[6] = "{\"meta\":{\"domain\":\"ru.wikipedia.org\"}}";
		messages[7] = "{\"meta\":{\"domain\":\"it.wikipedia.org\"}}";
		messages[8] = "{\"meta\":{\"domain\":\"es.wikipedia.org\"}}";
		messages[9] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";
		messages[10] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";
		messages[11] = "{\"meta\":{\"domain\":\"fr.wikipedia.org\"}}";
		messages[12] = "{\"meta\":{\"domain\":\"it.wikipedia.org\"}}";
		messages[13] = "{\"meta\":{\"domain\":\"ru.wikipedia.org\"}}";
		messages[14] = "{\"meta\":{\"domain\":\"es.wikipedia.org\"}}";
		messages[15] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";
		messages[16] = "{\"meta\":{\"domain\":\"fr.wikipedia.org\"}}";
		messages[17] = "{\"meta\":{\"domain\":\"au.wikipedia.org\"}}";
		messages[18] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";
		messages[19] = "{\"meta\":{\"domain\":\"en.wikipedia.org\"}}";

		try {
			for (int i = 0; i < messages.length; i++) {
				kafkaProducer.send(new ProducerRecord<Void, String>("wikimedia", null, messages[i]));
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.err.println("something went wrong... " + e.getMessage());
		} finally {
			kafkaProducer.close();
		}
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();
		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.VoidSerializer");
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		return producerProperties;
	}
}