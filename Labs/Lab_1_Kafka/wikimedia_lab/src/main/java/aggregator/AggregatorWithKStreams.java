package aggregator;

import static org.apache.kafka.streams.kstream.Suppressed.BufferConfig.unbounded;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.ForeachAction;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Suppressed;
import org.apache.kafka.streams.kstream.TimeWindowedKStream;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Window;
import org.apache.kafka.streams.kstream.Windowed;
/**
 * 
 *			WORK IN PROGRESS (maybe try with the Apache Flink framework?)
 *
 * @author Virginie Galtier
 *
 *         Reads the Kafka topic that publishes the locale of Wikipedia changes
 *         as strings, identifies and displays the top active regions on
 *         tumbling windows.
 */
public class AggregatorWithKStreams {

	/**
	 * Creates the aggregator (provoking infinite execution).
	 * 
	 * @param args first argument is a list of Kafka bootstrap servers, 
	 *             second argument is the name of the source Kafka topic, 
	 *             third argument is the size of the leader board (number of regions to display),
	 *             fourth argument is the duration of the observation window (in seconds)
	 */
	public static void main(String[] args) {
		new AggregatorWithKStreams(args[0], args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
	}

	/**
	 * 
	 * @param bootstrapServers List of Kafka bootstrap servers. Example:
	 *                         localhost:9092,another.host:9092
	 * @param regionTopicName  Name of the source Kafka topic
	 * @param leaderboardSize  Number of top regions to print
	 * @param windowsSec       Width of the tumbling window, in seconds
	 */
	public AggregatorWithKStreams(String bootstrapServers, String regionTopicName, int leaderboardSize, int windowsSec) {
		Topology regionCountTopology = createRegionCountTopology(regionTopicName, leaderboardSize, windowsSec);
		KafkaStreams regionCountStream = new KafkaStreams(regionCountTopology, configureRegionCount(bootstrapServers));
		final CountDownLatch latch = new CountDownLatch(1);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("streams-wordcount-shutdown-hook") {
			@Override
			public void run() {
				regionCountStream.close();
				latch.countDown();
			}
		});

		try {
			regionCountStream.start();
			latch.await();
		} catch (final Throwable e) {
			System.exit(1);
		}
		System.exit(0);
	}

	private Topology createRegionCountTopology(String regionTopicName, int leaderboardSize, int windowsSec) {

		StreamsBuilder builder = new StreamsBuilder();

		// key: void, value: region
		KStream<Void, String> regionStream = builder.stream(regionTopicName);

		// key: region, value: void
		KStream<String, Void> regionOneStream = regionStream
				.map(new KeyValueMapper<Void, String, KeyValue<String, Void>>() {
					@Override
					public KeyValue<String, Void> apply(Void keyRegion, String valueVoid) {
						return new KeyValue<String, Void>(valueVoid, keyRegion);
					}
				});

		// grouped by key = grouped by region
		KGroupedStream<String, Void> streamGroupedByRegion = regionOneStream
				.groupByKey(Grouped.with(Serdes.String(), Serdes.Void()));

		// window
		TimeWindowedKStream<String, Void> windowedStreamGroupedByRegion = streamGroupedByRegion
				.windowedBy(TimeWindows.ofSizeWithNoGrace(Duration.ofSeconds(windowsSec)));

		// key: window + region, value: nb of occurrences
		KTable<Windowed<String>, Long> tableCountPerRegionPerWindow = windowedStreamGroupedByRegion
				.count()
				.suppress(Suppressed.untilWindowCloses(unbounded()));
		
		// print leaderboard
		KStream<Windowed<String>, Long> streamCountPerRegionPerWindow = tableCountPerRegionPerWindow
				.toStream();
		streamCountPerRegionPerWindow.peek(new ForeachAction<Windowed<String>, Long>() {
			List<KeyValue<String, Long>> leaderBoard = new ArrayList<KeyValue<String, Long>>();
			List<KeyValue<String, Long>> lastLeaderBoard;
			Window lastWindow;

			@Override
			public void apply(Windowed<String> key, Long value) {
				// System.out.println("\ttotal in windows: " + key.key() + ": " + value + "
				// window: "
				// + key.window().startTime().toString() + " - " +
				// key.window().endTime().toString());
				if (lastWindow == null) {
					lastWindow = key.window();
					lastLeaderBoard = leaderBoard;
				}
				// add region
				leaderBoard.add(new KeyValue<String, Long>(key.key(), value));
				// sort leaderboard by decreasing nb of occurrences
				leaderBoard.sort(new Comparator<KeyValue<String, Long>>() {
					@Override
					public int compare(KeyValue<String, Long> o1, KeyValue<String, Long> o2) {
						return -1 * Long.compare(o1.value, o2.value);
					}
				});
				// keep leaderboard to NB_REGIONS_TO_LOG elements
				if (leaderBoard.size() > leaderboardSize) {
					leaderBoard.subList(leaderboardSize, leaderBoard.size()).clear();
				}
				// when a new window begins, print the previous one
				if (!lastWindow.equals(key.window())) {
					System.out.println("Leaderboard for window " + key.window().startTime().toString() + " - " + key.window().endTime().toString() + " : ");
					for (int i = 0; i < lastLeaderBoard.size(); i++) {
						System.out.println(lastLeaderBoard.get(i).key + " : " + lastLeaderBoard.get(i).value);
					}
					lastWindow = key.window();
					lastLeaderBoard = leaderBoard;
					leaderBoard = new ArrayList<KeyValue<String, Long>>();
				}
			}
		});

		return builder.build();

	}

	private Properties configureRegionCount(String bootstrapServers) {
		Properties regionCountStreamsProperties = new Properties();
		regionCountStreamsProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "regionCount");
		regionCountStreamsProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		// The semantics of caching is that data is flushed to the state store and
		// forwarded to the next downstream processor node whenever the earliest of
		// commit.interval.ms or cache.max.bytes.buffering (cache pressure) hits.
		regionCountStreamsProperties.put(StreamsConfig.STATESTORE_CACHE_MAX_BYTES_CONFIG, 0);
		regionCountStreamsProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		regionCountStreamsProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		return regionCountStreamsProperties;

	}
}
