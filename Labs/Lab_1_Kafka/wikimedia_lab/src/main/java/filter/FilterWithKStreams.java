package filter;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Predicate;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * 
 * @author Virginie Galtier
 * 
 *         Reads Wikimedia change events from a Kafka topic, keeps the ones that
 *         concern the Wikipedia project, and publishes for each of these the
 *         locale of the project to a different Kafka topic.
 */
public class FilterWithKStreams {
	/*
	 * List of Kafka bootstrap servers. Example: localhost:9092,another.host:9092
	 * 
	 * @see:
	 * https://jaceklaskowski.gitbooks.io/apache-kafka/content/kafka-properties-
	 * bootstrap-servers.html
	 */
	private String bootstrapServers;
	/*
	 * Name of the source Kafka topic
	 */
	private String wikimediaTopicName;
	/*
	 * Name of the destination Kafka topic
	 */
	private String regionTopicName;

	/**
	 * Creates a filter element (provoking infinite execution).
	 * 
	 * @param arg first argument is a list of Kafka bootstrap servers, second
	 *            argument is the name of the source Kafka topic, third argument is
	 *            the name of the destination Kafka topic
	 */
	public static void main(String[] arg) {
		new FilterWithKStreams(arg[0], arg[1], arg[2]);
	}

	/**
	 * Reads a wikimedia change event from a Kafka topic ; the locale of the event
	 * is extracted and published to the second topic (until the filter element is
	 * interrupted).
	 * 
	 * @param bootstrapServers   list of Kafka bootstrap servers. Example:
	 *                           localhost:9092,another.host:9092
	 * @param wikimediaTopicName name of the source Kafka topic
	 * @param regionTopicName    name of the destination Kafka topic
	 */
	FilterWithKStreams(String bootstrapServers, String wikimediaTopicName, String regionTopicName) {
		this.bootstrapServers = bootstrapServers;
		this.wikimediaTopicName = wikimediaTopicName;
		this.regionTopicName = regionTopicName;

		Topology wikipediaRegionTopology = createWikipediaRegionTopology();
		KafkaStreams wikipediaRegionStream = new KafkaStreams(wikipediaRegionTopology,
				configureWikipediaRegionKafkaStreams());
		wikipediaRegionStream.start();
	}

	private Topology createWikipediaRegionTopology() {
		StreamsBuilder builder = new StreamsBuilder();

		// creates a KStream from the input-topic
		// ---------------------------------------
		KStream<Void, String> source = builder.stream(wikimediaTopicName);

		// keeps only the wikipedia events -> filter
		// --------------------------------
		KStream<Void, String> wikipediaStream = source.filter(new Predicate<Void, String>() {
			@Override
			public boolean test(Void key, String value) {
				Gson gson = new Gson();
				JsonObject jsonObject = gson.fromJson(value, JsonObject.class);
				JsonObject jsonObjectMeta = jsonObject.getAsJsonObject("meta");
				String domain = jsonObjectMeta.getAsJsonPrimitive("domain").getAsString();
				return domain.contains("wikipedia");
			}
		});

		// keeps only the locale part of the 'domain' field -> map
		// -------------------------------------------------
		KStream<Void, String> regionStream = wikipediaStream
				.map(new KeyValueMapper<Void, String, KeyValue<Void, String>>() {
					@Override
					public KeyValue<Void, String> apply(Void key, String value) {
						Gson gson = new Gson();
						JsonObject jsonObject = gson.fromJson(value, JsonObject.class);
						JsonObject jsonObjectMeta = jsonObject.getAsJsonObject("meta");
						String domain = jsonObjectMeta.getAsJsonPrimitive("domain").getAsString();
						// example: domain = en.wikipedia.org
						String region = domain.split("\\.")[0]; // '.' is a special character in Java regex, must be
																// escaped
						return new KeyValue<>(key, region);
					}
				});
		// materializes this KStream to the output-topic
		regionStream.to(regionTopicName);

		return builder.build();
	}

	/**
	 * Prepares configuration for the Kafka stream
	 * 
	 * @return configuration properties for the Kafka stream
	 */
	private Properties configureWikipediaRegionKafkaStreams() {
		Properties streamsProperties = new Properties();
		streamsProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "wikipediaRegion");
		streamsProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		// The semantics of caching is that data is flushed to the state store and
		// forwarded to the next downstream processor node whenever the earliest of
		// commit.interval.ms or cache.max.bytes.buffering (cache pressure) hits.
		streamsProperties.put(StreamsConfig.STATESTORE_CACHE_MAX_BYTES_CONFIG, 0);
		streamsProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		streamsProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		streamsProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		return streamsProperties;
	}
}