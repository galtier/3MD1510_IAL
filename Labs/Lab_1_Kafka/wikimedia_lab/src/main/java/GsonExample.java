import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class GsonExample {
	public static void main(String[] args) {
		new GsonExample();
	}

	public GsonExample() {
		/*
		 {
  "employees": [
    {
      "name": "Mary",
      "hiredIn": 2008,
      "department": "Financial",
      "bonus": [
        {
          "2010": 1000
        }
      ]
    },
    {
      "name": "John",
      "hiredIn": 2010,
      "department": "Marketing",
      "bonus": [
        {
          "2015": 800
        },
        {
          "2017": 500
        },
        {
          "2019": 1000
        }
      ]
    },
    {
      "name": "Karl",
      "hiredIn": 2010,
      "department": "Design"
    }
  ]
}
		 */
		String json = "{ \"employees\": ["
				+ "{ \"name\": \"Mary\", \"hiredIn\": 2008, \"department\": \"Financial\", \"bonus\": [ { \"2010\": 1000 } ] },"
				+ "{ \"name\": \"John\", \"hiredIn\": 2010, \"department\": \"Marketing\", \"bonus\": [ { \"2015\": 800}, { \"2017\": 500}, { \"2019\": 1000}] },"
				+ "{ \"name\": \"Karl\", \"hiredIn\": 2010, \"department\": \"Design\" } ] }";

		// init Gson object
		Gson gson = new Gson();

		// deserialize the String to a JsonObject
		JsonObject jsonObject = gson.fromJson(json, JsonObject.class);

		// retrieve the "employees" list
		JsonArray employees = jsonObject.getAsJsonArray("employees");

		// iterate over the employees to find Mary
		for (JsonElement employeeElement : employees) {
			JsonObject employee = employeeElement.getAsJsonObject();
			String name = employee.get("name").getAsString();

			if (name.equals("Mary")) {
				// retrieve and print the department
				String department = employee.get("department").getAsString();
				System.out.println(department);
				break;
			}
		}
	}
}
